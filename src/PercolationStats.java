import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

/*-------------------------------------------------------------------------
 * Author: Spoorthi Bhat
 * Created date: 9/28/2017
 * Purpose: To perform percolation experiment on n-n grid the number of
 * times provided by the user and compute the Mean, Standard deviation and
 * 95% confidence interval.
 --------------------------------------------------------------------------*/
public class PercolationStats {

    /**
     * Mean computed.
     */
    private double meanComputed;

    /**
     * standard devisation computed.
     */
    private double standardDevComputed;

    /**
     * number of times percolation is performed.
     */
    private int trials;

    /**
     * an array to store each threshold value.
     */
    private final double[] threshold;

    /**
     * The constant value that is used for computation.
     */
    private final static double confidenceConstant = 1.96;
    /**
     * perform trials independent experiments on an n-by-n grid and compute
     * the percolation threshold for every trail.
     *
     * @param n - size of the grid.
     * @param trials - number of times percolation is performed.
     */
    public PercolationStats(int n, int trials) {

        if (n <= 0 || trials <= 0) {
            throw new IllegalArgumentException();
        }
        threshold = new double[trials];

        this.trials = trials;
        // perform percolation experiment trails number of times
        for (int i = 0; i < trials; i++) {
            Percolation percolation = new Percolation(n);
            while (!percolation.percolates()) {
                int randomRow = StdRandom.uniform(1, n + 1);
                int randonCol = StdRandom.uniform(1, n + 1);
                percolation.open(randomRow, randonCol);
            }
            // store the value of percolation threshold in an array
            threshold[i] = (double) percolation.numberOfOpenSites()
                    / (n * n);
        }
    }

    /**
     * Compute the sample mean of percolation threshold.
     *
     * @return the mean value.
     */
    public double mean() {
        if (meanComputed == 0) {
            meanComputed = StdStats.mean(threshold);
        }
        return meanComputed;
    }

    /**
     * Compute the sample standard deviation of percolation threshold.
     *
     * @return the stddev value.
     */
    public double stddev() {
        if (standardDevComputed == 0) {
            standardDevComputed = StdStats.stddev(threshold);
        }
        return standardDevComputed;
    }

    /**
     * Compute the low endpoint of 95% confidence interval.
     *
     * @return low endpoint value.
     */
    public double confidenceLo() {
        return mean() - (confidenceConstant * stddev() / Math.sqrt(trials));
    }

    /**
     * Compute the high endpoint of 95% confidence interval.
     *
     * @return high endpoint value.
     */
    public double confidenceHi() {
        return mean() + (confidenceConstant * stddev() / Math.sqrt(trials));
    }

    /**
     * Test client that takes two command line arguments,
     * n and number of trails.
     *
     * @param args - arg[0] takes size of grid and
     *             arg[1] takes number of trails.
     */
    public static void main(String[] args) {

        int gridN = Integer.parseInt(args[0]);
        int numberOftrials = Integer.parseInt(args[1]);

        PercolationStats stats = new PercolationStats(gridN, numberOftrials);

        System.out.println("% java PercolationStats "
                + gridN + " " + numberOftrials);
        System.out.println("Mean = " + stats.mean());
        System.out.println("StdDev = " + stats.stddev());
        System.out.println("95% confidence interval = ["
                + stats.confidenceLo() + " , " + stats.confidenceHi() + "]");
    }

}
