import edu.princeton.cs.algs4.WeightedQuickUnionUF;

/**---------------------------------------------------------------------------
 *  Author: Spoorthi Bhat
 * Written: 9/28/2017
 * Purpose: To to estimate the value of the percolation threshold via Monte
 * Carlo simulation.
 ---------------------------------------------------------------------------*/
public class Percolation {
    /**
     Stores the size of array.
     */
    private int sizeOfGrid;

    /**
     * Stores the index of topVirtualSite.
     */
    private int topVirtualSite;

    /**
     * Stores the index of bottomVirtualSite.
     */
    private int bottomVirtualSite;

    /**
     * Stores the count of openSites.
     */
    private int count;

    /**
     * Variable to indicate the state of site.
     */
    private boolean[] stateOfSite;

    /**
     * WeightedQuickUnion to compute union find from library.
     */
    private WeightedQuickUnionUF unionfind;

    /**
     * create sizeOfGrid-by-sizeOfGrid grid, with all sites blocked
     * Throws a IllegalArgumentException if sizeOfGrid is
     * less than or equal to 0.
     *
     * @param n - n to be initialized.
     */
    public Percolation(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("The value of sizeOfGrid "
                    + "cannot be zero or negative.");
        } else {
            this.sizeOfGrid = n;
            count = 0;
            // True -> open , False -> Blocked
            stateOfSite = new boolean[n * n + 2];
            topVirtualSite = 0;
            bottomVirtualSite = n * n + 1;
            unionfind = new WeightedQuickUnionUF(n * n + 2);
            stateOfSite[topVirtualSite] = true;
            stateOfSite[bottomVirtualSite] = true;

            for (int i = 1; i <= n * n; i++) {
                // Set all sites to blocked
                // (if stateOfsite is false then the site is blocked)
                stateOfSite[i] = false;
            }
        }
    }

    /**
     * To determine the index of the array by the values of row and col.
     *
     * @param row - row  index to be translated.
     * @param col - column index to be translated.
     * @return the value of index
     */
    private int index(final int row, final int col) {
        if (row < 1 || row > sizeOfGrid || col < 1 || col > sizeOfGrid) {
            throw new IllegalArgumentException("Out of range");
        } else {
            return sizeOfGrid * (row - 1) + col;
        }
    }

    /**
     * Check whether the site is a top site.
     *( i.e., a site in the topmost row of the grid)
     * @param row - row to be checked.
     * @param col - column to be checked.
     * @return true-> if the site if a top site, otherwise -> false
     */
    private boolean isTop(final int row, final int col) {
        if (row < 1 || row > sizeOfGrid || col < 1 || col > sizeOfGrid) {
            throw new IllegalArgumentException("Out of range");
        } else {
            return (index(row, col) <= sizeOfGrid);
        }
    }

    /**
     * Check whether the site is a bottom site
     *( i.e., a site in the last row of the grid).
     * @param row - row to be checked for bottom.
     * @param col - column to be checked for bottom.
     * @return true-> if the site if a bottom site, otherwise -> false.
     */
    private boolean isBottom(final int row, final int col) {
        if (row < 1 || row > sizeOfGrid || col < 1 || col > sizeOfGrid) {
            throw new IllegalArgumentException("Out of range");
        } else {
            return (index(row, col)
                    >= (sizeOfGrid * sizeOfGrid) - sizeOfGrid + 1);
        }
    }

    /**
     * open site (row, col) if it is not open already.
     * After opening, connect with the adjacent open sites.
     * If the opened site is a top site or bottom site,
     * connect it to top virtual site and bottom virtual site
     * respectively.
     * Throws a IllegalArgumentException if
     * row and col are not in prescribed range.
     *
     * @param row - specific row that contains the site to be opened.
     * @param col - specific column that contains the site to be opened.
     */
    public void open(int row, int col) {
        if (row < 1 || row > sizeOfGrid || col < 1 || col > sizeOfGrid) {
            throw new IllegalArgumentException("Out of range");
        } else if (!isOpen(row, col)) {
            stateOfSite[index(row, col)] = true;
            count++; // to count the number of sites opened
            // connect this this with adjacent sites if they are open
            if (col < sizeOfGrid && isOpen(row, col + 1)) {
                unionfind.union(index(row, col), index(row, col + 1));
            }
            if (col > 1 && isOpen(row, col - 1)) {
                unionfind.union(index(row, col), index(row, col - 1));
            }
            if (row < sizeOfGrid && isOpen(row + 1, col)) {
                unionfind.union(index(row, col), index(row + 1, col));
            }
            if (row > 1 && isOpen(row - 1, col)) {
                unionfind.union(index(row, col), index(row - 1, col));
            }

            if (isTop(row, col)) {
                unionfind.union(index(row, col), topVirtualSite);
            }
            if (isBottom(row, col)) {
                unionfind.union(index(row, col), bottomVirtualSite);
            }
        }
    }

    /**
     * Checks whether the site is open or not.
     * Throws a IllegalArgumentException if
     * row and col are not in prescribed range.
     *
     * @param row - row of the site to be known if open or not.
     * @param col - column of the site to be known if open or not.
     * @return true-> site is open.
     * false-> site is not open.
     */
    public boolean isOpen(int row, int col) {
        if (row < 1 || row > sizeOfGrid || col < 1 || col > sizeOfGrid) {
            throw new IllegalArgumentException("Out of range");
        } else {
            return stateOfSite[index(row, col)];
        }
    }

    /**
     * Checks whether the site is full or not
     * (i.e., open site connected to top virtual site) .
     * Throws a IllegalArgumentException if
     * row and col are not in prescribed range.
     *
     * @param row - row of the site to be known if full or not.
     * @param col - column of the site to be known if full or not.
     * @return true -> is full.
     * false -> is not full.
     */
    public boolean isFull(int row, int col) {
        if (row < 1 || row > sizeOfGrid || col < 1 || col > sizeOfGrid) {
            throw new IllegalArgumentException("Out of range");
        } else {
            return (unionfind.connected(index(row, col), topVirtualSite));
        }
    }

    /**
     * Determine the number of open sites.
     *
     * @return the number of open sites.
     */
    public int numberOfOpenSites() {
        // number of open sites
        return count;
    }

    /**
     * Checks whether the system percolates or not.
     *
     * @return true -> the system percolates.
     * false -> the system does not percolate.
     */
    public boolean percolates() {

        return (unionfind.connected(topVirtualSite, bottomVirtualSite));
    }

    /**
     * test client
     *
     * @param args - args[0] gives n.
     */
    /* public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        Percolation percolation = new Percolation(n);
        int c = 0;
        while (!percolation.percolates()) {
            if (c==231) {
               c= 231;
            }
            int randomRow = StdRandom.uniform(1, n + 1);
            int randonCol = StdRandom.uniform(1, n + 1);
            percolation.open(randomRow, randonCol);
            c++;
        }
        System.out.println(
        percolation.numberOfOpenSites());
        float percolationThreshold
        = (float) percolation.numberOfOpenSites() / n*n;
        System.out.println("The percolation threshold is "
        + percolationThreshold);
    }*/
}
