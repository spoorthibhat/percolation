
import java.io.BufferedReader;
	import java.io.FileReader;
	import java.util.ArrayList;
	import java.util.List;

	public class ReadFile
	{
	    private String filePath;
	    private List<String> inputFileContents;
	    private int index;

	    int n;

	    /**
	     * Constructor which reads the filepath to be read.
	     * @param filename
	     */
	    public ReadFile(String filepath)
	    {
	        this.filePath = filepath;
	        inputFileContents = new ArrayList<String>();
	        readAllContentsFromFile();
	    }

	    /**
	     * Method when called returns next pair of x and y from the file.
	     * @return
	     */
	    public int[] getNextPairs()
	    {
	        String currentLine = inputFileContents.get(index++);
	        int arr[] = new int[2];
	        arr[0] = Integer.parseInt(currentLine.trim().split("\\s++")[0]);
	        arr[1] = Integer.parseInt(currentLine.trim().split("\\s++")[1]);;
	        return arr;
	    }

	    /**
	     * Method to read all the lines in a file into a private arraylist.
	     */
	    private void readAllContentsFromFile(){
	        try {
	        	BufferedReader br = new BufferedReader(new FileReader(filePath));
	        	String line;
	            boolean isFirstlineRead = false;
	            while ((line = br.readLine()) != null) {
	                if (!isFirstlineRead) {
	                    n = Integer.parseInt(line.trim());
	                    isFirstlineRead = true;
	                } else {
	                    inputFileContents.add(line);
	                }
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }

	    public static void main(String args[])
	    {
//	        //Sample Usage:-
//	        ReadFile rf = new ReadFile("/Users/spoor/Downloads/percolation-testing/percolation/input20.txt");
//
//	        /*
//	          Wherever you want to read a next line from the file, just call rf.getNextPairs() which returns an array of next pairs.
//	         */
//	        for (int i = 0 ; i<rf.n ; i++) { //To read first 50 lines.
//	            int arr[] = rf.getNextPairs();
//	            System.out.println(arr[0] + " and " + arr[1]);
//	        }

	    }

	}

