package com.tests;

public class SuccessorWithDelete {

	int set [];
	int n;
	
	public SuccessorWithDelete(int n){
		 set = new int[n];
		 for(int i = 0; i<n ; i++){
			 set[i] = i;
		 }
	}
	
	private int root(int x){
		while(x!=set[x]){
			set[x] = set[set[x]];
			x= set[x];
		}
		return x;
	}
	public void removeX(int x){
		
		int p = root(x);
		int q = root(x+1);
		set[p] = q;
	}
		
	public static void main(String[] args) {
		
		SuccessorWithDelete myset = new SuccessorWithDelete(5);
		myset.removeX(2);
		
		System.out.println(myset.root(2));
	}
}
