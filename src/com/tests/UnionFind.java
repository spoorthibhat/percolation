package com.tests;

public class UnionFind {

	int id[];
	int N;
	int size[];
	
	public UnionFind(int N) {
		id = new int[N];
		size = new int[N];
		for(int i= 0; i < N; i++){
			id[i] = i;
		    size[i] = 1; 
		}
	}
	
	private int root(int i){
		
		while(i != id[i]){
			id[i] = id[id[i]];
			i = id[i];
		}
		
		return i;
	}
	
	public int find(int i){
		
		int largest = i;
		for(int j = 0 ; j< N; j++){
			if(root(id[j])== root(i)){
				if(j > i){
					largest = j;
				}
			}
		}
		return largest;
	}
	
	public boolean connected(int p , int q){
		return (root(p) == root(q));
	}
	
	public void union(int p , int q){
		int i = root(p);
		int j = root(q);
		if(size[i] < size[j]){
			id[i] = j;
			size[j] += size[i];
		}else{
			id[j] =  i;
			size[i] +=size[j];
		}
		
	}
}
